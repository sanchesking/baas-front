React.render(
  <h1>Hello, world!</h1>,
  document.getElementById('content')
);

var ProjectItem = React.createClass({
  render: function() {
    var project = this.props.project,
      title = project.title;

    return (
      <div className="note-summary">{title}</div>
    );
  }
});

var NotesList = React.createClass({
  getInitialState: function() {
    return {
      projects: []
    };
  },

  componentDidMount: function() {
    var endPointRoute = 'http://localhost:3000/api/v1/';
    var actionRoute = 'projects';
    var dataParams = JSON.stringify({});
    //var actionRoute = 'projects/6';
    //var dataParams = JSON.stringify({project:{title:'title11', description:'description'}});
    //var actionRoute = 'auth';
    //var dataParams = JSON.stringify({config_name: "default", confirm_success_url: "http://localhost:8080/", email: "sancheskinghtc@gmail.com", password: "12345678", password_confirmation: "12345678"});

    $.ajax({
      type: 'GET',
      url: endPointRoute + actionRoute,
      contentType: "application/json; charset={CHARSET}",
      dataType: 'json',
      data: dataParams,
      headers: window.loginHeaders,
      success: function(data) {
        if (this.isMounted()) {
          this.setState({
            projects: data
          });
        }
      }.bind(this),
      error: function(data) {
        alert(data.status + ' ' + data.statusText);
      }
    });
  },
  render: function() {
    var projects = this.state.projects;

    return (
      <div className="note-list">
        {
          projects.map(function(project) {
            return (
              <ProjectItem key={project.id} project={project}/>
            );
          })
        }
      </div>
      );
    }
});

$('.render-projects').click(function() {
  React.render(
  <NotesList />,
    document.getElementById('projects')
  );
});
