(function() {

	window.loginHeaders = {};
	this.auth = function() {
		var endPointRoute = 'http://localhost:3000/api/v1/';
		var actionRoute = 'auth/sign_in';
		var dataParams = JSON.stringify({password: "12345678", email: "sancheskinghtc@gmail.com"});

		$.ajax({
			type: 'POST',
			url: endPointRoute + actionRoute,
			contentType: "application/json; charset={CHARSET}",
			dataType: 'json',
			data: dataParams,
			success: function(data) {
        alert('Successful Authentication!');
				console.log(data);
			},
			error: function(data) {
				alert(data.status + ' ' + data.statusText);
			},
			complete: function(resp){
		        window.loginHeaders['Access-Token'] = resp.getResponseHeader('Access-Token');
		        window.loginHeaders.Client = resp.getResponseHeader('Client');
		        window.loginHeaders.Uid = resp.getResponseHeader('Uid');
		    }
		});
	}

})();